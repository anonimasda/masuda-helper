import { MasdaManager } from "./modules/masda-manager.js"
import "./modules/components/masda-helper.js"
import { vglobal } from "./modules/vglobal.js"

const mm = new MasdaManager()
vglobal.mm = mm

mm.prepare().then(ok => {
	if(ok) {
		chrome.tabs.query({ active: true, currentWindow: true }, tabs => {
			if(!tabs[0].url) return
			const url = new URL(tabs[0].url)
			if(url.origin === "https://anond.hatelabo.jp"){
				const title = "anond:" + url.pathname.slice(1)
				document.querySelector("masda-helper").restore(title, "")
			} else if(url.origin.startsWith("http")){
				document.querySelector("masda-helper").restore(url.href, "")
			}
		})
	} else {
		alert("ログインしていません")
	}
})
