export function esc(text){
	const escape = {
		"&": "&amp;",
		"<": "&lt;",
		">": "&gt;",
		'"': "&quot;",
	}
	return text.replace(/[&<>"]/g, ch => escape[ch])
}

const html_raw_symbol = Symbol("html.raw")

export function raw(html){
	return {[html_raw_symbol]: html}
}

export function html(parts, ...values){
	return parts.reduce((acc, part) => {
		let value = values.shift()
		if(value instanceof Object){
			value = value[html_raw_symbol] || ""
		}else{
			value = esc(String(value))
		}
		return acc + value + part
	})
}
