import { CustomHTMLElement } from "../custom-html-element.js"
import * as hh from "../html-helper.js"
import { vglobal } from "../vglobal.js"

const { esc, raw, html } = hh

customElements.define(
	"panel-draft",
	class extends CustomHTMLElement {
		constructor() {
			super()
			this.shadowRoot.addEventListener("click", event => {
				const li = event.target.closest("li")
				if (!li) return

				if (event.target.closest(".delete")) {
					vglobal.mm.draft.remove(li.dataset.key)
					this.reload()
				} else {
					this.dispatchEvent(new CustomEvent("restore", { bubbles: true, detail: li.dataset.key }))
				}
			})
		}

		get template() {
			return html`
				<style>
					* {
						box-sizing: border-box;
					}

					#list {
						margin: 0;
						padding: 0;
						list-style: none;
					}

					li {
						margin: 5px 0;
						padding: 5px;
						border-bottom: 1px solid #5279e7;
						cursor: pointer;
					}

					h1 {
						font-size: 15px;
						margin: 5px 0;
						white-space: nowrap;
						overflow: hidden;
						text-overflow: ellipsis;
					}

					h1 span {
						color: crimson;
					}

					p {
						margin: 5px 0;
						white-space: nowrap;
						overflow: hidden;
						text-overflow: ellipsis;
					}

					a {
						color: #5279e7;
					}

					footer {
						text-align: right;
						margin-right: 10px;
					}
					
					footer span {
						font-size: 10px;
						color: gray;
					}
					
					footer input {
						margin-left: 5px;
					}
				</style>
				<ul id="list">
					${raw(this.getListHTML())}
				</ul>
			`
		}
		
		reload(){
			this.elements.list.innerHTML = this.getListHTML()
		}
		
		getListHTML(){
			return vglobal.mm.draft.drafts
				.sort((a, b) => {
					if (a.auto === b.auto) {
						return b.lastUpdated - a.lastUpdated
					} else {
						return a.auto - b.auto
					}
				})
				.map(item => {
					return html`
						<li data-key="${item.key}">
							<h1>${item.auto ? raw("<span>[auto] </span>") : ""}${item.title}</h1>
							<p>${item.body}</p>
							<footer>
								<span>Saved at: ${new Date(item.lastUpdated).toLocaleString()}</span>
								<input class="delete" type="button" value="削除">
							</footer>
						</li>
					`
				})
				.join("")
		}
	}
)
