import { CustomHTMLElement } from "../custom-html-element.js"
import { ExtendableTimer } from "../extendable-timer.js"
import * as hh from "../html-helper.js"
import { vglobal } from "../vglobal.js"

const { esc, raw, html } = hh

customElements.define(
	"panel-edit",
	class extends CustomHTMLElement {
		constructor() {
			super()
			this.lock = false

			this.elements.draft.addEventListener("click", event => {
				vglobal.mm.draft.add(this.elements.title.value, this.elements.body.value)
				alert("保存しました")
			})

			this.elements.submit.addEventListener("click", async event => {
				if (this.lock) {
					alert("投稿処理中です")
					return
				}
				if(confirm("投稿してもよろしいですか？")){
					this.lock = true
					await vglobal.mm.post(this.elements.title.value, this.elements.body.value)
					alert("投稿しました\n一覧 から確認してください")
					this.lock = false
				}
			})

			let prev = { title: "", body: "" }
			this.shadowRoot.addEventListener("input", event => {
				if (this.timer && !this.timer.done) {
					this.timer.extend(3000)
				} else {
					this.timer = new ExtendableTimer(() => {
						const title = this.elements.title.value
						const body = this.elements.body.value
						
						if(prev.title === title && prev.body === body){
							return
						}
						vglobal.mm.draft.add(this.elements.title.value, this.elements.body.value, true)
						prev = { title, body }
					}, 3000)
				}
			})
		}

		disconnectedCallback() {
			this.timer && this.timer.immediate()
		}

		get template() {
			return html`
				<style>
					* {
						box-sizing: border-box;
					}

					ul {
						margin: 0;
						padding: 0;
						list-style: none;
					}

					#root {
						height: 100%;
						display: flex;
						flex-flow: column nowrap;
					}

					#root>div {
						padding: 5px;
					}

					#root>div:nth-child(2) {
						flex: 1 0 0;
					}

					#root>div:nth-child(3) {
						text-align: right;
					}

					#title, #body {
						display: block;
						width: 100%;
						height: 100%;
						border: 1px solid #5279e7;
						padding: 5px 10px;
						resize: none;
						font-size: 14px;
						font-family: meiryo;
					}
				</style>
				<section id="root">
					<div><input id="title"></div>
					<div><textarea id="body"></textarea></div>
					<div>
						<input id="draft" type="button" value="下書き">
						<input id="submit" type="button" value="投稿">
					</div>
				</section>
			`
		}

		restore(title, body) {
			this.elements.title.value = title
			this.elements.body.value = body
		}
	}
)
