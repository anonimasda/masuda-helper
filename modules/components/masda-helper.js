import { CustomHTMLElement } from "../custom-html-element.js"
import "./panel-edit.js"
import "./panel-draft.js"
import "./panel-list.js"
import * as hh from "../html-helper.js"
import { vglobal } from "../vglobal.js"

const { esc, raw, html } = hh

customElements.define(
	"masda-helper",
	class extends CustomHTMLElement {
		constructor() {
			super()
			this.active = "edit"

			this.elements.opentab.addEventListener("click", event => {
				window.open(location.href)
			})

			this.elements.side.addEventListener("click", event => {
				const btn = event.target.closest("[data-panel]")
				if (!btn) return

				this.switchPanel(btn.id)
			})

			this.shadowRoot.addEventListener("restore", event => {
				const draft = vglobal.mm.draft.get(event.detail)
				this.restore(draft.title, draft.body)
			})
			
			this.switchPanel("edit")
		}

		get template() {
			return html`
				<style>
					* {
						box-sizing: border-box;
					}

					:host {
						width: 100vw;
						height: 100vh;
						min-width: 600px;
						min-height: 400px;
						display: flex;
						flex-flow: row nowrap;
						align-items: stretch;
					}
					
					#side {
						width: 60px;
						flex: none;
						background: #484848;
						color: #f0f0f0;
						border-right: 1px solid #333;
						padding: 5px;
						position: relative;
					}

					#side div {
						margin: 15px 0;
						cursor: pointer;
					}

					#side div.active {
						font-weight: bold;
						font-size: 1.05em;
						color: #fafafa;
						text-shadow: 1px 1px 0px rgba(255,255,255,0.4);
					}

					#opentab {
						position: absolute;
						bottom: 0;
						font-size: 22px;
						border: 2px solid #aaa;
						border-radius: 4px;
						width: 30px;
						height: 30px;
						display: flex;
						align-items: center;
						justify-content: center;
					}

					#main {
						width: calc(100% - 60px);
						flex: none;
						padding: 10px;
					}
					
					#main>* {
						width: 100%;
						height: 100%;
						overflow: auto;
						display: block;
					}
				</style>
				<aside id="side">
					<div id="edit" data-panel="panel-edit">書く</div>
					<div id="draft" data-panel="panel-draft">下書</div>
					<div id="list" data-panel="panel-list">一覧</div>
					<div id="opentab" title="タブで開く">➚</div>
				</aside>
				<main id="main"></main>
			`
		}
		
		restore(title, body) {
			this.switchPanel("edit")
			const edit_panel = this.elements.main.firstElementChild
			edit_panel.restore(title, body)
		}

		switchPanel(name) {
			const old_btn = this.elements[this.active]
			const new_btn = this.elements[name]

			old_btn.classList.remove("active")
			new_btn.classList.add("active")
			this.active = name
			this.elements.main.innerHTML = `<${new_btn.dataset.panel}></${new_btn.dataset.panel}>`
		}
	}
)
