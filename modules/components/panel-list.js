import { CustomHTMLElement } from "../custom-html-element.js"
import * as hh from "../html-helper.js"
import { vglobal } from "../vglobal.js"

const { esc, raw, html } = hh

customElements.define(
	"panel-list",
	class extends CustomHTMLElement {
		constructor() {
			super()
			!(async () => {
				const my_entries = await vglobal.mm.getMyEntries()
				this.shadowRoot.innerHTML = html`
					<style>
						* {
							box-sizing: border-box;
						}

						ul {
							margin: 0;
							padding: 0;
							list-style: none;
						}

						li {
							margin: 5px 0;
							padding: 5px;
							border-bottom: 1px solid #5279e7;
						}

						h1 {
							font-size: 15px;
							margin: 5px 0;
							white-space: nowrap;
							overflow: hidden;
							text-overflow: ellipsis;
						}

						p {
							margin: 5px 0;
							white-space: nowrap;
							overflow: hidden;
							text-overflow: ellipsis;
						}

						a {
							color: #5279e7;
						}

						div {
							text-align: right;
							margin-right: 10px;
						}
					</style>

					<ul>
						${raw(my_entries
							.map(item => {
								return html`
									<li>
										<h1><a href="${item.href}" target="_blank">${item.title}</a></h1>
										<div>Trackback: ${item.tb}</div>
									</li>
								`
							})
							.join(""))}
					</ul>
				`
			})()
		}
	}
)
