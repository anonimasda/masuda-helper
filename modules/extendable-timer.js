/**
 * 遅延タイマー
 */
class ExtendableTimer {
	constructor(fn, msec) {
		this.done = false
		this.callback = () => {
			if (!this.done) {
				fn()
			}
			this.done = true
		}
		this.timer = setTimeout(this.callback, msec)
	}

	cancel() {
		clearInterval(this.timer)
	}

	extend(msec) {
		clearInterval(this.timer)
		this.timer = setTimeout(this.callback, msec)
	}

	immediate() {
		clearInterval(this.timer)
		this.callback()
	}
}

export { ExtendableTimer }
