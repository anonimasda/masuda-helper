/**
 * カスタムエレメントの共通ベースクラス
 */
class CustomHTMLElement extends HTMLElement {
	constructor() {
		super()
		this.attachShadow({ mode: "open" }).innerHTML = this.template
	}

	get template() {
		return ""
	}

	get elements() {
		const elements = Array.from(this.shadowRoot.querySelectorAll("[id]"))
			.reduce((acc, element) => ({ ...acc, [element.id]: element }), {})

		Object.defineProperty(this, "elements", {
			value: elements,
		})
		return elements
	}
}
export { CustomHTMLElement }
