import { Draft } from "./draft.js"

/**
 * 増田マネージャ
 */
class MasdaManager {
	constructor() {
		this.urls = {
			top: "https://anond.hatelabo.jp/",
		}

		this.draft = new Draft()
	}

	/**
	 * URL のセット
	 * ログインしてたら true
	 * @returns boolean
	 */
	async prepare() {
		await this._setURLs()

		return Object.keys(this.urls).length > 1
	}

	/**
	 * URL → document
	 * @param url
	 */
	async _parsePage(url) {
		const res = await fetch(url, { credentials: "include" })
		const html = await res.text()
		return new DOMParser().parseFromString(html, "text/html")
	}

	/**
	 * マイページなどの URL 取得・ this.urls に追加
	 */
	async _setURLs() {
		const doc = await this._parsePage(this.urls.top)
		const menu_links = [...doc.querySelectorAll("#globalheader a")]
		// ログアウトがみつからないのはログインしていない状態
		if (!menu_links.find(e => e.innerHTML.trim() === "ログアウト")) {
			return {}
		}
		const my_page = menu_links.find(e => e.innerHTML.trim().endsWith("の日記")).getAttribute("href")
		const edit_page = menu_links.find(e => e.innerHTML.trim() === "日記を書く").getAttribute("href")

		Object.assign(this.urls, {
			my_page: new URL(my_page, this.urls.top).href,
			edit_page: new URL(edit_page, this.urls.top).href,
		})
	}

	/**
	 * iframe から増田へ post する
	 *
	 * @param title
	 * @param body
	 */
	async post(title, body) {
		if (!this.urls.edit_page) {
			alert("ログインしていません")
			return
		}
		const doc = await this._parsePage(this.urls.edit_page)
		const form = doc.querySelector("#body form")
		form.action = this.urls.edit_page
		doc.querySelector("#text-title").value = title
		doc.querySelector("#text-body").value = body.replace(/^$\n/gm, "<br>\n")

		const frame = document.createElement("iframe")
		frame.hidden = true
		document.body.append(frame)
		frame.contentDocument.body.append(form)

		await new Promise(resolve => {
			frame.onload = resolve
			frame.contentDocument.querySelector(`input[value="この内容を登録する"]`).click()
		})

		frame.remove()
	}

	/**
	 * マイエントリ取得
	 */
	async getMyEntries() {
		if (!this.urls.my_page) {
			alert("ログインしていません")
			return
		}
		const doc = await this._parsePage(this.urls.my_page)
		return Array.from(doc.querySelectorAll(".sanchor"), element => {
			const title = element.closest("h3").textContent
			const href = new URL(element.closest("a").getAttribute("href"), this.urls.top).href
			const tb = element
				.closest(".section")
				.querySelector(".sectionfooter")
				.textContent.match(/記事への反応\(([0-9]+)\)/)[1]
			return { title, tb, href }
		})
	}
}

export { MasdaManager }
