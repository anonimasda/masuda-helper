/**
 * ドラフト
 */
class Draft {
	constructor() {
		this.drafts = this.load()
	}

	/**
	 * オートセーブ保存数
	 */
	get max_auto_save(){
		return 100
	}

	/**
	 * localStorage からドラフト読み込み
	 *
	 * @returns {Array}
	 */
	load() {
		return JSON.parse(localStorage.drafts || "[]")
	}

	/**
	 * localStorage にドラフト保存
	 */
	save() {
		localStorage.drafts = JSON.stringify(this.drafts)
	}

	/**
	 * ドラフトに追加
	 *
	 * @param title
	 * @param body
	 */
	add(title, body, is_auto) {
		// 最大数を超える分は削除
		if(is_auto){
			const auto_saved = this.drafts.filter(draft => draft.is_auto)
			if(auto_saved.length >= this.max_auto_save){
				auto_saved.sort((a, b) => b.lastUpdated - a.lastUpdated)
				for(const {key} of auto_saved.slice(this.max_auto_save)){
					this.remove(key)
				}
			}
		}
		this.drafts.push({
			title,
			body,
			lastUpdated: Date.now(),
			key:
				"K-" +
				Math.random()
					.toString(16)
					.substr(2),
			auto: !!is_auto,
		})
		this.save()
	}

	/**
	 * ドラフトから削除
	 *
	 * @param key
	 */
	remove(key) {
		const index = this.drafts.findIndex(item => item.key === key)
		~index && this.drafts.splice(index, 1)
		this.save()
	}

	/**
	 * 指定キーのドラフトを取得
	 *
	 * @param key
	 */
	get(key) {
		return this.drafts.find(item => item.key === key)
	}
}

export { Draft }
